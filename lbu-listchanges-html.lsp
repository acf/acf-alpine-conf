<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% htmlviewfunctions.displaycommandresults({"commit"}, session, true) %>

<%
	viewlibrary.dispatch_component("status")
	if viewlibrary.check_permission("commit") then
		viewlibrary.dispatch_component("commit")
	end
%>

<%
if viewlibrary.check_permission("getpackage") then
	local viewtype = cfe({type="hidden", value="stream"})
	htmlviewfunctions.displayitem(cfe({type="form", value={viewtype=viewtype}, label="Download Overlay", option="Download", action="getpackage" }), page_info)
end
%>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
	<% htmlviewfunctions.displayitemstart() %>
	Files changed since last commit
	<% htmlviewfunctions.displayitemmiddle() %>
	<pre><%
		if (#view.value == 0) then
			io.write("None")
		else
			for i,file in pairs(view.value) do
				io.write((html.html_escape(file.status .. "\t" .. file.name .. "\n")))
			end
		end
	%></pre>
	<% htmlviewfunctions.displayitemend() %>
<% htmlviewfunctions.displaysectionend(header_level) %>
