local mymodule = {}

mymodule.default_action = "listchanges"

function mymodule.status (self)
	return self.model.getstatus()
end

function mymodule.listchanges (self)
	return self.model.list()
end

function mymodule.config (self)
	return self.handle_form(self, self.model.getconfig, self.model.setconfig, self.clientdata, "Save", "Edit Config", "Configuration Set")
end

function mymodule.editincluded (self)
	return self.handle_form(self, self.model.getincluded, self.model.setincluded, self.clientdata, "Save", "Edit Included Files", "Included files set")
end

function mymodule.editexcluded (self)
	return self.handle_form(self, self.model.getexcluded, self.model.setexcluded, self.clientdata, "Save", "Edit Excluded Files", "Excluded files set")
end

function mymodule.commit(self)
	return self.handle_form(self, self.model.getcommit, self.model.commit, self.clientdata, "Commit", "Commit Changes")
end

function mymodule.expert (self)
	return self.handle_form(self, self.model.get_filedetails, self.model.set_filedetails, self.clientdata, "Save", "Edit Config File", "Configuration saved")
end

function mymodule.listbackups(self)
	return self.model.getbackupfiles()
end

function mymodule.selectbackup(self)
	return self.handle_form(self, self.model.get_selectbackup, self.model.selectbackupfile, self.clientdata, "Revert")
end

function mymodule.getpackage(self)
	return self.model.getpackage()
end

return mymodule
