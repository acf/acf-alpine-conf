<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#backuplist").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
	});
</script>

<% htmlviewfunctions.displaycommandresults({"selectbackup"}, session) %>

<% local header_level = htmlviewfunctions.displaysectionstart(view, page_info) %>
<% if #view.value == 0 then %>
<p>No backup files</p>
<% else %>
	<table id="backuplist" class="tablesorter"><thead>
	<tr>
		<th>Action</th>
		<th>File</th>
	</tr>
	</thead><tbody>
	<% for i,file in ipairs(view.value) do %>
		<tr><td>
			<% htmlviewfunctions.displayitem(cfe({type="link", value={backup=cfe({ type="hidden", value=file.filename })}, label="", option="Revert", action="selectbackup"}), page_info, -1) %>
		</td>
		<td><%= html.html_escape(file.filename) %></td>
		<td><span class="hide"><%= html.html_escape(file.size or 0) %>b</span><%= format.formatfilesize(file.size) %></td>
		<td><%= format.formattime(file.mtime) %></td>
	<% end %>
	</tbody></table>
<% end %>
<% htmlviewfunctions.displaysectionend(header_level) %>
