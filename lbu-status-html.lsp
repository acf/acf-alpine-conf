<% local view, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
%>

<%
if view.value.committed.value then
	view.value.committed.value = "There are no uncommitted files"
else
	view.value.committed.value = "WARNING!\nUntil you commit, you will lose your changes at next reboot/shutdown!"
end
view.value.committed.type = "text"

htmlviewfunctions.displayitem(view, page_info)
%>
