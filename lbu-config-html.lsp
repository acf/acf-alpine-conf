<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
%>

<% htmlviewfunctions.displaycommandresults({"editincluded", "editexcluded", "selectbackup"}, session, true) %>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("status")
end %>

<%
htmlviewfunctions.displayitem(form, page_info)

if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("editincluded")
	viewlibrary.dispatch_component("editexcluded")
	viewlibrary.dispatch_component("listbackups")
end
%>
